package com.dzmytrovych.movieland.controller;


import com.dzmytrovych.movieland.entity.Movie;
import com.dzmytrovych.movieland.service.MovieService;
import com.dzmytrovych.movieland.service.dto.MovieView;
import com.dzmytrovych.movieland.util.JsonJacksonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/v1/movie")
public class MovieController {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private MovieService movieService;

    @Autowired
    private JsonJacksonConverter jsonJacksonConverter;

    @RequestMapping(produces = "text/plain;charset=UTF-8")

    @ResponseBody
    public String getAllMovie() {
        log.info("Sending request to get all movie");
        long startTime = System.currentTimeMillis();
        List<Movie> movies = movieService.getAll();
        List<MovieView> movieViews = new ArrayList<MovieView>();
        for (Movie movie : movies){
            movieViews.add(new MovieView(movie));
        }
        String movieJson = jsonJacksonConverter.toJson(movieViews);
        log.info("City {} is received. It took {} ms",  System.currentTimeMillis() - startTime);
        return movieJson;
    }

    }


