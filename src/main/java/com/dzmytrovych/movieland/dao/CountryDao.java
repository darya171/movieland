package com.dzmytrovych.movieland.dao;

import com.dzmytrovych.movieland.entity.Country;

import java.util.List;

public interface CountryDao {
    public List<Country> getAll();
}
