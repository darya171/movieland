package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.entity.Country;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CountryRowMapper  implements RowMapper<Country> {

    @Override
    public Country mapRow(ResultSet resultSet, int i) throws SQLException {
        Country country = new Country();
        country.setName(resultSet.getString("name"));
        country.setId(resultSet.getInt("country_id"));

        return country;
    }
}

