package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.dao.GenreDao;
import com.dzmytrovych.movieland.entity.Genre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class GenreDaoImpl implements GenreDao{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getAllGenresSQL;

    @Override
    public List<Genre> getAll() {
        log.info("Start query to get all genres from DB");
        long startTime = System.currentTimeMillis();
        List<Genre> genres;// = new ArrayList<>();
        genres = jdbcTemplate.query(getAllGenresSQL, new GenreRowMapper());
        log.info("Finish query to get all genres from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return genres;
    }
}