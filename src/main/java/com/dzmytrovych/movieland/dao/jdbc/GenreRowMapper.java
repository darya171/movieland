package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.entity.Genre;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenreRowMapper implements RowMapper<Genre> {

    @Override
    public Genre mapRow(ResultSet resultSet, int i) throws SQLException {
        Genre genre = new Genre();
        genre.setName(resultSet.getString("name"));
        genre.setId(resultSet.getInt("genre_id"));

        return genre;
    }
}

