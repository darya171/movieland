package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.dao.MovieDao;
import com.dzmytrovych.movieland.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieDaoImpl implements MovieDao{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getAllMovieSQL;

    @Override
    public List<Movie> getAll() {
        log.info("Start query to get all movies from DB");
        long startTime = System.currentTimeMillis();
        List<Movie> movies; // = new ArrayList<>();
        movies = jdbcTemplate.query(getAllMovieSQL, new MovieRowMapper());
        log.info("Finish query to get all movies from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return movies;
    }
}