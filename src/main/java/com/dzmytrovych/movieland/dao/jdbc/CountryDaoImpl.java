package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.dao.CountryDao;
import com.dzmytrovych.movieland.entity.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CountryDaoImpl implements CountryDao{

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private String getAllCountrySQL;

    @Override
    public List<Country> getAll() {
        log.info("Start query to get all countries from DB");
        long startTime = System.currentTimeMillis();
        List<Country> countries;// = new ArrayList<>();
        countries = jdbcTemplate.query(getAllCountrySQL, new CountryRowMapper());
        log.info("Finish query to get all countries from DB. It took {} ms", System.currentTimeMillis() - startTime);
        return countries;
    }
}
