package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.entity.Country;
import com.dzmytrovych.movieland.entity.Genre;
import com.dzmytrovych.movieland.entity.Movie;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MovieRowMapper implements RowMapper<Movie>{

    @Override
    public Movie mapRow(ResultSet resultSet, int i) throws SQLException {
        Movie movie = new Movie();
        movie.setNameRus(resultSet.getString("name_rus"));
        movie.setNameEng(resultSet.getString("name_eng"));
        movie.setYear(resultSet.getInt("year"));
        movie.setDescription(resultSet.getString("description"));
        movie.setTotalRate(resultSet.getDouble("total_rate"));
        movie.setPrice(resultSet.getDouble("price"));
        movie.setId(resultSet.getInt("film_id"));
        //get country IDs for each movie - START
            String countryIDs[] = resultSet.getString("movie_countries").split(", ");
            List<Country> movieCountries = new ArrayList<>();
            for (String id : countryIDs) {
                Country country = new Country();
                country.setId(Integer.valueOf(id));
                movieCountries.add(country);
            }
            movie.setCountries(movieCountries);
        //get country IDs for each movie - END

        //get genre IDs for each movie - START
        String genreIDs[] = resultSet.getString("movie_genres").split(", ");
        List<Genre> movieGenres = new ArrayList<>();
        for (String id : genreIDs) {
            Genre genre = new Genre();
            genre.setId(Integer.valueOf(id));
            movieGenres.add(genre);
        }
        movie.setGenres(movieGenres);
        //get genre IDs for each movie - END

        return movie;
    }
}
