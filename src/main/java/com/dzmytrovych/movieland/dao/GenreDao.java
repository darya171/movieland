package com.dzmytrovych.movieland.dao;

import com.dzmytrovych.movieland.entity.Genre;

import java.util.List;

public interface GenreDao {
    public List<Genre> getAll();
}