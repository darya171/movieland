package com.dzmytrovych.movieland.dao;

import com.dzmytrovych.movieland.entity.Movie;

import java.util.List;

public interface MovieDao {
    public List<Movie> getAll();
}
