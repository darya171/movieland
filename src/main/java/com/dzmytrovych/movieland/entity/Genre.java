package com.dzmytrovych.movieland.entity;

/**
 * Created by Red2 on 13.06.2016.
 */
public class Genre {
    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
