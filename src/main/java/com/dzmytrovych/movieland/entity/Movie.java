package com.dzmytrovych.movieland.entity;

import java.util.List;

/**
 * Created by Red2 on 10.06.2016.
 */
public class Movie {
    private String nameRus;
    private String nameEng;
    private int year;
    private List<Country> countries;
    private List<Genre> genres;
    private String description;
    private double totalRate;
    private double price;
    private int id;

    @Override
    public String toString() {
        return "Movie{" +
                "nameRus='" + nameRus + '\'' +
                ", year=" + year +
                ", countries='" + countries + '\'' +
                ", genres=" + genres +
                ", description='" + description + '\'' +
                ", totalRate=" + totalRate +
                ", price=" + price +
                '}';
    }

    public String getNameRus() {
        return nameRus;
    }

    public void setNameRus(String nameRus) {
        this.nameRus = nameRus;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(double totalRate) {
        this.totalRate = totalRate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

