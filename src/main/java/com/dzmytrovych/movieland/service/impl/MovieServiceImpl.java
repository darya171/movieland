package com.dzmytrovych.movieland.service.impl;

import com.dzmytrovych.movieland.dao.MovieDao;
import com.dzmytrovych.movieland.entity.Movie;
import com.dzmytrovych.movieland.service.MovieService;
import com.dzmytrovych.movieland.service.MovieEnrich;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService{

    @Autowired
    private MovieDao movieDao;

    @Autowired
    private MovieEnrich movieEnrich;

    @Override
    public List<Movie> getAll() {
        List<Movie> movieList = movieDao.getAll();
        movieEnrich.getMovieDetails(movieList);

        return movieList;
    }
}
