package com.dzmytrovych.movieland.service;

import com.dzmytrovych.movieland.dao.jdbc.GenreDaoImpl;
import com.dzmytrovych.movieland.entity.Country;
import com.dzmytrovych.movieland.entity.Genre;
import com.dzmytrovych.movieland.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import com.dzmytrovych.movieland.dao.jdbc.CountryDaoImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class MovieEnrich {

    @Autowired
    private CountryDaoImpl countryDaoImpl;
    @Autowired
    private GenreDaoImpl genreDaoImpl;

    public Movie getMovieDetails (Movie movie){
        movie.setCountries(getCountryListNames(movie.getCountries()));
        movie.setGenres(getGenreListNames(movie.getGenres()));
        return movie;
    }

    public List<Movie> getMovieDetails (List<Movie> movies){
        for (Movie movie : movies) {
            getMovieDetails (movie);
        }
        return movies;
    }

    private List<Country> getCountryListNames(List<Country> countryMovieList){
        List<Country> countryDBList = countryDaoImpl.getAll();
        //store country dictionary in a HashMap
        HashMap <Integer, String> countries = new HashMap<>();
                for (Country country : countryDBList){
            countries.put(country.getId(), country.getName());
        }
        //set country names for movie by IDs
        for (Country country : countryMovieList){
            country.setName(countries.get(country.getId()));
        }
        return countryMovieList;
    }
    private List<Genre> getGenreListNames(List<Genre> genreMovieList){
        List<Genre> genreDBList = genreDaoImpl.getAll();
        //store genre dictionary in a HashMap
        HashMap <Integer, String> genres = new HashMap<>();
        for (Genre genre : genreDBList){
            genres.put(genre.getId(), genre.getName());
        }
        //set genre names for movie by IDs
        for (Genre genre : genreMovieList){
            genre.setName(genres.get(genre.getId()));
        }
        return genreMovieList;
    }
}
