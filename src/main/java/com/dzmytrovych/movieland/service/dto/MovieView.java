package com.dzmytrovych.movieland.service.dto;

import com.dzmytrovych.movieland.entity.Country;
import com.dzmytrovych.movieland.entity.Genre;
import com.dzmytrovych.movieland.entity.Movie;

import java.util.ArrayList;
import java.util.List;

public class MovieView {
    private String nameRus;
    private String nameEng;
    private int year;
    private List<String> countries;
    private List<String> genres;
    private String description;
    private double totalRate;
    private double price;
    private int id;

    public MovieView(String nameRus, String nameEng, int year, List<String> countries, List<String> genres, String description, double totalRate, double price, int id) {
        this.nameRus = nameRus;
        this.nameEng = nameEng;
        this.year = year;
        this.countries = countries;
        this.genres = genres;
        this.description = description;
        this.totalRate = totalRate;
        this.price = price;
        this.id = id;
    }
    public MovieView(Movie movie) {
        this.nameRus = movie.getNameRus();
        this.nameEng = movie.getNameEng();
        this.year = movie.getYear();
        this.countries = toCountryNameList(movie.getCountries());
        this.genres = toGenreNameList(movie.getGenres());
        this.description = movie.getDescription();
        this.totalRate = movie.getTotalRate();
        this.price = movie.getPrice();
        this.id = movie.getId();
    }
    private List<String> toCountryNameList(List<Country> countryList){
        List<String> countryNames = new ArrayList<>();
        for (Country country : countryList){
            countryNames.add(country.getName());
        }
        return countryNames;
    }
    private List<String> toGenreNameList(List<Genre> genreList){
        List<String> genreNames = new ArrayList<>();
        for (Genre genre : genreList){
            genreNames.add(genre.getName());
        }
        return genreNames;
    }

    public String getNameRus() {
        return nameRus;
    }

    public void setNameRus(String nameRus) {
        this.nameRus = nameRus;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(double totalRate) {
        this.totalRate = totalRate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MovieView{" +
                "nameRus='" + nameRus + '\'' +
                ", nameEng='" + nameEng + '\'' +
                ", year=" + year +
                ", countries=" + countries +
                ", genres=" + genres +
                ", description='" + description + '\'' +
                ", totalRate=" + totalRate +
                ", price=" + price +
                ", id=" + id +
                '}';
    }
}
