package com.dzmytrovych.movieland.service;

import com.dzmytrovych.movieland.entity.Movie;

import java.util.List;

public interface MovieService {
    public List<Movie> getAll();
}
