package com.dzmytrovych.movieland.PrepareData;

import java.io.File;
import java.io.IOException;
import java.util.*;
import static com.dzmytrovych.movieland.PrepareData.PostgreSQLJDBC.runSQL;

public class UploadData {

    public static void main(String[] args) {
        /*runSQL(recreateMovieSchema());
        runSQL(saveGenre("genre.txt"));
        runSQL(saveMovie(parseMovie("movie.txt")));
        runSQL(saveUser("user.txt"));
        runSQL(saveReview("review.txt"));*/
        System.out.println(recreateMovieSchema());
        System.out.println(saveGenre("genre.txt"));
        System.out.println(saveMovie(parseMovie("movie.txt")));
        System.out.println(saveUser("user.txt"));
        System.out.println(saveReview("review.txt"));
    }

    private static ArrayList<String> getFile(String fileName) {
        ArrayList<String> list = new ArrayList<>();
        //Get file from resources folder
        ClassLoader classLoader = UploadData.class.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                //result.append(line).append("\n");
                list.add(line);
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ArrayList<Movie> parseMovie(String fileName) {
        ArrayList<Movie> movies = new ArrayList<>();
        Movie m = new Movie();
        ArrayList<String> movieList = getFile(fileName);
        for (int i = 0; i < movieList.size(); i++) {
            String s = movieList.get(i);
            if (i % 8 == 0) {
                m = new Movie();
            }
            switch (i % 8) {
                case 0:
                    m.setNameRus(s.split("/")[0]);
                    m.setNameEng(s.split("/")[1]);
                    break;
                case 1:
                    m.setYear(Integer.parseInt(s));
                    break;
                case 2:
                    List<String> movie_countries = new ArrayList<>();
                    String countries[] = s.split(", ");
                    for (String g : countries) {
                        movie_countries.add(g);
                    }
                    m.setCountries(movie_countries);
                    break;
                case 3:
                    List<String> movie_genres = new ArrayList<>();
                    String genres[] = s.split(", ");
                    for (String g : genres) {
                        movie_genres.add(g);
                    }
                    m.setGenres(movie_genres);
                    break;
                case 4:
                    m.setDescription(s);
                    break;
                case 5:
                    m.setTotalRate(Double.valueOf(s.split(":")[1]));
                    break;
                case 6:
                    m.setPrice(Double.valueOf(s.split(":")[1]));
                    break;
            }
            if (i % 8 == 6) {
                movies.add(m);
                System.out.println(m);
            }
        }
        return movies;
    }

    public static String saveMovie(ArrayList<Movie> movieList) {
        String result = "";
        String movie_fields = "(name_rus, name_eng, year, description, total_rate, price)";
        result += saveCountry(movieList);
        for (Movie m : movieList) {
            String values =
                    "'" + m.getNameRus().replace("'", "''") + "', '"
                            + m.getNameEng().replace("'", "''") + "', "
                            + m.getYear() + ","
                            + "'" + m.getDescription().replace("'", "''") + "', "
                            + m.getTotalRate() + ","
                            + m.getPrice();
            result += "insert into movie_admin.movie " + movie_fields + " values (" + values + ");" + '\n';
            for (String genreName : m.getGenres()) {
                result += "insert into movie_admin.movie_genre (film_id, genre_id) \n" +
                        " values (( select film_id from movie_admin.movie m where m.name_rus = '"
                        + m.getNameRus().replace("'", "''") + "'),\n"
                        + "(select genre_id from movie_admin.genre g where g.name = '"
                        + genreName
                        + "'));" + '\n';
            }
            for (String countryName : m.getCountries()) {
                result += "insert into movie_admin.movie_country (film_id, country_id) \n" +
                        " values (( select film_id from movie_admin.movie m where m.name_rus = '"
                        + m.getNameRus().replace("'", "''") + "'),\n"
                        + "(select country_id from movie_admin.country g where g.name = '"
                        + countryName
                        + "'));" + '\n';
            }
        }
        System.out.println("movieSQL = [" + result + "]");
        return result;
    }

    public static String saveCountry(ArrayList<Movie> movieList) {
        String result = "";
        HashSet<String> countries = new HashSet<>();
        for (Movie m : movieList) {
            for (String country : m.getCountries()) {
                countries.add(country);
            }
        }
        for (String country : countries) {
            result += "insert into movie_admin.country (name) " +
                    " values ('"
                    + country + "');\n";
        }
        return result;
    }

    public static String saveGenre(String fileName) {
        ArrayList<String> genres = getFile(fileName);
        String sql = "";
        for (String s : genres) {
            sql += "INSERT INTO genre (NAME) "
                    + "VALUES ('" + s + "');" + '\n';
        }
        return sql;
    }

    public static String saveUser(String fileName) {
        ArrayList<String> userList = getFile(fileName);
        String result = "";
        String name = "";
        String mail = "";
        String nick = "";
        for (int i = 0; i < userList.size(); i++) {
            String s = userList.get(i);
            switch (i % 4) {
                case 0:
                    name = s;
                    break;
                case 1:
                    mail = s;
                    break;
                case 2:
                    nick = s;
                    break;
            }
            if (i % 4 == 2) {
                result += "insert into movie_admin.user (name, mail, nick) values ('" +
                        name + "', '" +
                        mail + "', '" +
                        nick + "');"
                        + '\n';
            }
        }
        return result;
    }

    public static String saveReview(String fileName) {
        ArrayList<String> reviewList = getFile(fileName);
        String result = "";
        String film = "";
        String user = "";
        String review = "";
        for (int i = 0; i < reviewList.size(); i++) {
            String s = reviewList.get(i);
            switch (i % 4) {
                case 0:
                    film = s;
                    break;
                case 1:
                    user = s;
                    break;
                case 2:
                    review = s;
                    break;

            }
            if (i % 4 == 2) {
                result += "insert into movie_admin.review (film_id, user_id, review) values (" +
                        "\n" +
                        " ( select film_id from movie_admin.movie m where m.name_rus = '" +
                        film.replace("'", "''") +
                        "' ),\n" +
                        " ( select user_id from movie_admin.user u where u.name_rus = '" +
                        user +
                        "'),\n" + "'" +
                        review.replace("'", "''") + "');"
                        + '\n';
            }
        }
        return result;
    }

    @Deprecated
    private static HashMap<String, Integer> getGenre() {
/*
        Connection c = null;
        Statement stmt = null;
        HashMap<String, Integer> genres = new HashMap<>();
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "movie_admin", "123");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM movie_admin.genre;");
            while (rs.next()) {
                *//*int id = rs.getInt("id");
                String  name = rs.getString("name");*//*
                genres.put(rs.getString("name"), rs.getInt("genre_id"));
                *//*System.out.println( "ID = " + id );
                System.out.println( "NAME = " + name );
                System.out.println();*//*
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");*/

        return null;
    }

    public static String recreateMovieSchema() {
/*CREATE USER movie_admin WITH password '123';
GRANT ALL privileges ON DATABASE postgres TO movie_admin;

CREATE SCHEMA movie_admin
AUTHORIZATION movie_admin;

GRANT CONNECT ON DATABASE postgres to movie_admin;
GRANT USAGE ON SCHEMA public to movie_admin;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA movie_admin TO movie_admin;
GRANT SELECT ON ALL TABLES IN SCHEMA movie_admin TO movie_admin;
GRANT insert ON ALL TABLES IN SCHEMA movie_admin TO movie_admin;
GRANT delete ON ALL TABLES IN SCHEMA movie_admin TO movie_admin;
GRANT update ON ALL TABLES IN SCHEMA movie_admin TO movie_admin;*/

        String createSchema = "" + "drop table movie_admin.genre;\n" +
                "        CREATE TABLE movie_admin.genre (\n" +
                "                genre_id  serial NOT NULL primary key,\n" +
                "                name          VARCHAR(50)\n" +
                "        )\n" +
                "        ;\n" +
                "        ALTER TABLE movie_admin.genre\n" +
                "        OWNER TO movie_admin;" + "drop table movie_admin.movie;\n" +
                " CREATE TABLE movie_admin.movie\n" +
                "(\n" +
                "   name_rus character varying(1000), \n" +
                "   name_eng character varying(1000), \n" +
                "   year integer, \n" +
                "   country_origin character(50), \n" +
                "   description character varying(4000), \n" +
                "   total_rate double precision, \n" +
                "   price double precision, \n" +
                "   creation_date date default current_date, \n" +
                "   film_id serial NOT NULL primary key\n" +
                ") \n" +
                "WITH (\n" +
                "  OIDS = FALSE\n" +
                ")\n" +
                ";\n" +
                "ALTER TABLE movie_admin.movie\n" +
                "  OWNER TO movie_admin;" + "drop table movie_admin.movie_genre;\n" +
                " CREATE TABLE movie_admin.movie_genre\n" +
                "( \n" +
                "   id serial NOT NULL primary key,\n" +
                "   film_id integer, \n" +
                "   genre_id integer\n" +
                ") \n" +
                "WITH (\n" +
                "  OIDS = FALSE\n" +
                ")\n" +
                ";\n" +
                "ALTER TABLE movie_admin.movie_genre\n" +
                "  OWNER TO movie_admin;" + "drop table movie_admin.user;\n" +
                " CREATE TABLE movie_admin.user\n" +
                "( \n" +
                "   user_id serial NOT NULL primary key,\n" +
                "   name character varying(150),\n" +
                "   mail character varying(50),\n" +
                "   nick character varying(50)\n" +
                ") \n" +
                "WITH (\n" +
                "  OIDS = FALSE\n" +
                ")\n" +
                ";\n" +
                "ALTER TABLE movie_admin.user\n" +
                "  OWNER TO movie_admin;" +
                "        drop table movie_admin.country;\n" +
                "        CREATE TABLE movie_admin.country\n" +
                "                (\n" +
                "                        country_id serial NOT NULL primary key,\n" +
                "                        name character varying(150)\n" +
                "                )\n" +
                "        WITH (\n" +
                "                OIDS = FALSE\n" +
                "        )\n" +
                "        ;\n" +
                "        ALTER TABLE movie_admin.country\n" +
                "        OWNER TO movie_admin;\n" +
                "        drop table movie_admin.movie_country;\n" +
                "        CREATE TABLE movie_admin.movie_country\n" +
                "                (\n" +
                "                        id serial NOT NULL primary key,\n" +
                "                        film_id integer,\n" +
                "                        country_id integer\n" +
                "                )\n" +
                "        WITH (\n" +
                "                OIDS = FALSE\n" +
                "        )\n" +
                "        ;\n" +
                "        ALTER TABLE movie_admin.movie_country\n" +
                "        OWNER TO movie_admin;\n" +
                "drop table movie_admin.review;\n" +
                " CREATE TABLE movie_admin.review\n" +
                "( \n" +
                "   review_id serial NOT NULL primary key,\n" +
                "   film_id integer, \n" +
                "   user_id integer,\n" +
                "   review character varying(4000)\n" +
                ") \n" +
                "WITH (\n" +
                "  OIDS = FALSE\n" +
                ")\n" +
                ";\n" +
                "ALTER TABLE movie_admin.review\n" +
                "  OWNER TO movie_admin;"
                + "";
        return createSchema;
    }
    private static class Movie {
        private String nameRus;
        private String nameEng;
        private int year;
        private List<String> countries;
        private List<String> genres;
        private String description;
        private double totalRate;
        private double price;

        @Override
        public String toString() {
            return "Movie{" +
                    "nameRus='" + nameRus + '\'' +
                    "nameEng='" + nameEng + '\'' +
                    ", year=" + year +
                    ", countries='" + countries + '\'' +
                    ", genres=" + genres +
                    ", description='" + description + '\'' +
                    ", totalRate=" + totalRate +
                    ", price=" + price +
                    '}';
        }

        public String getNameRus() {
            return nameRus;
        }

        public void setNameRus(String name) {
            this.nameRus = name;
        }

        public String getNameEng() {
            return nameEng;
        }

        public void setNameEng(String nameEng) {
            this.nameEng = nameEng;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public List<String> getCountries() {
            return countries;
        }

        public void setCountries(List<String> countries) {
            this.countries = countries;
        }

        public List<String> getGenres() {
            return genres;
        }

        public void setGenres(List<String> genres) {
            this.genres = genres;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public double getTotalRate() {
            return totalRate;
        }

        public void setTotalRate(double totalRate) {
            this.totalRate = totalRate;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }


    }

}