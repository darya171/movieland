package com.dzmytrovych.movieland.dao.jdbc;

import com.dzmytrovych.movieland.entity.Country;
import com.dzmytrovych.movieland.entity.Genre;
import com.dzmytrovych.movieland.entity.Movie;
import org.junit.Test;

import java.sql.ResultSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieRowMapperTest {

    @Test
    public void testMapRow() throws Exception {

        ResultSet resultSet = mock(ResultSet.class);

        when(resultSet.getString("name_rus")).thenReturn("MovieTestName");
        when(resultSet.getString("name_eng")).thenReturn("MovieTestName");
        when(resultSet.getInt("year")).thenReturn(1991);
        when(resultSet.getString("movie_countries")).thenReturn("1, 2");
        when(resultSet.getString("movie_genres")).thenReturn("5, 6");
        when(resultSet.getString("description")).thenReturn("MovieDescription");
        when(resultSet.getDouble("total_rate")).thenReturn(99.7);
        when(resultSet.getDouble("price")).thenReturn(213.4);
        when(resultSet.getInt("film_id")).thenReturn(1);
        when(resultSet.getString("")).thenReturn("");

        MovieRowMapper movieRowMapper = new MovieRowMapper();
        Movie movie = movieRowMapper.mapRow(resultSet, 0);
        assertEquals(movie.getNameRus(), "MovieTestName");
        assertEquals(movie.getNameEng(), "MovieTestName");
        assertEquals(movie.getYear(), 1991);
        assertEquals(movie.getCountries().get(0).getId(), 1);
        assertEquals(movie.getCountries().get(1).getId(), 2);
        assertEquals(movie.getGenres().get(0).getId(), 5);
        assertEquals(movie.getGenres().get(1).getId(), 6);
        assertEquals(movie.getDescription(), "MovieDescription");
        assertEquals(movie.getId(), 1);
    }
}